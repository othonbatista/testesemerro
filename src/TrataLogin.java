

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/TrataLogin")
public class TrataLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TrataLogin() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
		String usuarioNome = request.getParameter("nome");
		String usuarioSenha = request.getParameter("senha");
		
		final PrintWriter saida = response.getWriter();
		
		if (usuarioNome.equalsIgnoreCase("fulano") &&
			usuarioSenha.equalsIgnoreCase("senha")) {
			
			saida.println ("<h1 align='center'>Senha Correta!!!</h1>");
			
		} else {
			
			saida.println ("<h1 align='center'>Senha Incorreta!!!</h1>");
			
		}
		
	}

}
